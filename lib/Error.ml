
module Signature = struct
  type 'a t = exn

  let map _ s = s
end

module Error = Free.Make (Signature)
include Error

let err e = Error.op e

let op e = raise e

let run m = Error.run {return = Fun.id; op = op} m
