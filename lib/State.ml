
module Make (S : sig
  type t
end) =
struct
  module Signature = struct
    type 'a t =
      | Get of (S.t -> 'a)
      | Set of S.t * (unit -> 'a)

    let map f s =
      match s with
      | Get g -> Get (fun s -> f (g s))
      | Set (s,g) -> Set (s, fun () -> f (g ()))
  end

  module FreeState = Free.Make (Signature)
  include FreeState

  let get () = op @@ Get Fun.id

  let set s = op @@ Set (s, Fun.id)

  let ret a _ = a

  let op st s =
    match st with
    | Signature.Get f -> f s s
    | Signature.Set (_, f) -> f () s

  let run m = FreeState.run {return = ret; op = op} m
end
