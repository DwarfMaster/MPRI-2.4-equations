
module Make (Env : sig
  type t
end) =
struct
  module Base = struct
    type 'a t = Env.t -> 'a

    let return a = Fun.const a

    let bind (m : 'a t) (f : 'a -> 'b t) = fun e -> f (m e) e
  end

  module M = Monad.Expand (Base)
  include M

  let get () = Fun.id

  let run m = m
end
