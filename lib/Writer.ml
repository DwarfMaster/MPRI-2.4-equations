module Make (Log : sig
  type t

  val empty : t

  val ( <+> ) : t -> t -> t
end) =
struct
  open Log

  module Base = struct
    type 'a t = Log.t * 'a

    let return a = (empty, a)

    let bind (l1,a) f = let (l2,b) = f a in (l1 <+> l2, b)
  end

  module M = Monad.Expand (Base)
  include M

  let set l = (l,())

  let run m = m
end
